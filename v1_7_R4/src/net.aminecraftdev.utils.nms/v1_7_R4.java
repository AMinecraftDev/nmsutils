package net.aminecraftdev.utils.nms;

import net.aminecraftdev.utils.reflection.ReflectionHandler;
import net.minecraft.server.v1_7_R4.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 09-Nov-17
 */
public class v1_7_R4 implements ReflectionHandler {

    @Override
    public String getVersion() {
        return getClass().getSimpleName();
    }

    @Override
    public void sendActionBar(Player player, String s) {
        if(player == null) return;
        if(s == null) return;

        CraftPlayer craftPlayer = (CraftPlayer) player;

        if(craftPlayer.getHandle().playerConnection.networkManager.getVersion() != 47) return;

        IChatBaseComponent chatBaseComponent = ChatSerializer.a("{\"text\": \"" + s + "\"}");
        PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(chatBaseComponent);
        PlayerConnection playerConnection = craftPlayer.getHandle().playerConnection;

        playerConnection.sendPacket(packetPlayOutChat);
    }

    @Override
    public void sendActionBar(Player player, String s, int i, Plugin plugin) {
        sendActionBar(player, s);

        if(i >= 0) {
            Bukkit.getScheduler().runTaskLater(plugin, () -> sendActionBar(player, ""), i+1);
        }

        while(i > 60) {
            i -= 60;
            int schedule = i % 60;

            Bukkit.getScheduler().runTaskLater(plugin, () -> sendActionBar(player, s), (long) schedule);
        }
    }

    @Override
    public void sendActionBarAll(String s, Plugin plugin) {
        sendActionBarAll(s, -1, plugin);
    }

    @Override
    public void sendActionBarAll(String s, int i, Plugin plugin) {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            sendActionBar(player, s, i, plugin);
        }
    }

    @Override
    public boolean isChunkLoaded(Location location) {
        if(location == null) return false;
        if(location.getWorld() == null) return false;

        int x = location.getBlockX() / 16;
        int z = location.getBlockZ() / 16;
        CraftWorld craftWorld = (CraftWorld) location.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        ChunkProviderServer chunkProviderServer = worldServer.chunkProviderServer;

        return chunkProviderServer.isChunkLoaded(x, z);
    }

    @Override
    public void loadChunk(Location location) {
        if(isChunkLoaded(location)) return;

        int x = location.getBlockX() / 16;
        int z = location.getBlockZ() / 16;
        CraftWorld craftWorld = (CraftWorld) location.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        ChunkProviderServer chunkProviderServer = worldServer.chunkProviderServer;

        chunkProviderServer.loadChunk(x, z);
    }

    @Override
    public void setSpawnerValues(Block block, int spawnAmount) {
        CraftWorld craftWorld = (CraftWorld) block.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        TileEntity tileEntity = worldServer.getTileEntity(block.getX(), block.getY(), block.getZ());

        if(!(tileEntity instanceof TileEntityMobSpawner)) return;

        TileEntityMobSpawner tileEntityMobSpawner = (TileEntityMobSpawner) tileEntity;
        NBTTagCompound compound = new NBTTagCompound();

        tileEntityMobSpawner.b(compound);
        compound.setShort("SpawnCount", (short) spawnAmount);
        compound.setShort("MaxNearbyEntities", (short) 5000);
        tileEntityMobSpawner.a(compound);
    }
}