package net.aminecraftdev.utils.reflection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 10-Nov-17
 */
public class ReflectionUtils extends NMSVersionHandler {

    private static ReflectionHandler reflectionHandler;
    private static double nmsDoubleVersion = -1.0;
    private static boolean useOldMethods = false;

    public ReflectionUtils() {
        if(nmsDoubleVersion == -1) {
            String str1 = Bukkit.getServer().getClass().getPackage().getName();
            String[] array = str1.replace(".", ",").split(",")[3].split("_");
            String str2 = array[0].replace("v", "");
            String str3 = array[1];

            nmsDoubleVersion = Double.parseDouble(str2 + "." + str3);
        }

        if(reflectionHandler == null) {
            try {
                reflectionHandler = (ReflectionHandler) Class.forName(getClass().getName().replace("reflection.ReflectionUtils", "") + "nms." + getAPIVersion()).newInstance();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }

            if (getAPIVersion().equalsIgnoreCase("v1_8_R1") || getAPIVersion().startsWith("v1_7_")) { // Not sure if 1_7 works for the protocol hack?
                useOldMethods = true;
            }

            logDebug("Loading support for " + getAPIVersion() + ".");
        }
    }

    public static Class<?> getNMSClass(String path) {
        try {
            return Class.forName("net.minecraft.server." + getAPIVersion() + "." + path);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static Class<?> getOBCClass(String path) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + getAPIVersion() + "." + path);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static boolean isUseOldMethods() {
        return useOldMethods;
    }

    public static double getDoubleNMSVersion() {
        return nmsDoubleVersion;
    }

    public static ReflectionHandler getReflectionHandler() {
        return reflectionHandler;
    }

    private static void logDebug(String log) {
        log("&d[DEBUG] &7" + log);
    }

    private static void log(String log) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', log));
    }

}
