package net.aminecraftdev.utils.reflection;

import org.bukkit.potion.PotionEffectType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Nov-17
 */
public class PotionReflection extends ReflectionUtils {

    public static PotionEffectType getGlowing() {
        if(getAPIVersion().startsWith("v1_8") || getAPIVersion().startsWith("v1_7")) {
            return PotionEffectType.getByName("night_vision");
        } else {
            return PotionEffectType.getByName("glowing");
        }
    }

}
