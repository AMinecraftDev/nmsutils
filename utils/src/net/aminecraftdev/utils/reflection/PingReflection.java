package net.aminecraftdev.utils.reflection;

import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 15-Dec-17
 */
public class PingReflection extends ReflectionUtils {

    private static Method getHandleMethod;
    private static Field pingField;

    public static int getPing(Player player) {
        try {
            if(getHandleMethod == null) {
                getHandleMethod = player.getClass().getDeclaredMethod("getHandle");
                getHandleMethod.setAccessible(true);
            }

            Object entityPlayer = getHandleMethod.invoke(player);

            if(pingField == null) {
                pingField = entityPlayer.getClass().getDeclaredField("ping");
                pingField.setAccessible(true);
            }

            int ping = pingField.getInt(entityPlayer);

            return ping > 0? ping : 0;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        return 0;
    }

}
