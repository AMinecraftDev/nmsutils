package net.aminecraftdev.utils.reflection;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 25-Aug-17
 */
public interface ReflectionHandler {

    String getVersion();

    void setSpawnerValues(Block block, int spawnAmount);

    boolean isChunkLoaded(Location location);

    void loadChunk(Location location);

    void sendActionBar(Player player, String message);

    void sendActionBar(Player player, String message, int duration, Plugin plugin);

    void sendActionBarAll(String message, Plugin plugin);

    void sendActionBarAll(String message, int duration, Plugin plugin);

}