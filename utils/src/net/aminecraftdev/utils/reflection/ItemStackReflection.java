package net.aminecraftdev.utils.reflection;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Method;
import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Nov-17
 */
public class ItemStackReflection extends ReflectionUtils {

    private static Class<?> craftItemStackClazz, nmsItemStackClazz, nmsItemClazz, localeClazz, nbtTagCompoundClazz;
    private static Method nmsCopy, saveNms;

    public static String getJsonItemStack(ItemStack itemStack) {
        try {
            if(craftItemStackClazz == null) craftItemStackClazz = getOBCClass("inventory.CraftItemStack");
            if(nmsCopy == null) nmsCopy = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);
            if(nmsItemStackClazz == null) nmsItemStackClazz = getNMSClass("ItemStack");
            if(nbtTagCompoundClazz == null) nbtTagCompoundClazz = getNMSClass("NBTTagCompound");
            if(saveNms == null) saveNms = nmsItemStackClazz.getMethod("save", nbtTagCompoundClazz);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
        Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method
        Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

        try {
            nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
            nmsItemStackObj = nmsCopy.invoke(null, itemStack);
            itemAsJsonObject = saveNms.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
        } catch (Throwable t) {
            Bukkit.getLogger().log(Level.SEVERE, "failed to serialize itemstack to nms item", t);
            return null;
        }

        // Return a string representation of the serialized object
        return itemAsJsonObject.toString();
    }

    public static String getFriendlyName(Material material) {
        return material == null ? "Air" : getFriendlyName(new ItemStack(material), false);
    }

    public static String getFriendlyName(ItemStack itemStack, boolean checkDisplayName) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return "Air";

        try {
            if(craftItemStackClazz == null) craftItemStackClazz = getOBCClass("inventory.CraftItemStack");
            if(nmsItemStackClazz == null) nmsItemStackClazz = getNMSClass("ItemStack");
            if(nmsItemClazz == null) nmsItemClazz = getNMSClass("Item");
            if(localeClazz == null) localeClazz = getNMSClass("LocaleI18n");
            if(nmsCopy == null) nmsCopy = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);

            Object nmsItemStack = nmsCopy.invoke(null, itemStack);
            Object itemName;

            if(checkDisplayName) {
                Method getName = nmsItemStackClazz.getMethod("getName");

                itemName = getName.invoke(nmsItemStack);
            } else {
                Method getItem = nmsItemStackClazz.getMethod("getItem");
                Object nmsItem = getItem.invoke(nmsItemStack);
                Method getName = nmsItemClazz.getMethod("getName");
                Object localItemName = getName.invoke(nmsItem);
                Method getLocale = localeClazz.getMethod("get", String.class);
                Object localeString = localItemName == null? "" : getLocale.invoke(null, localItemName);

                itemName = ("" + getLocale.invoke(null, localeString.toString() + ".name")).trim();
            }

            return itemName != null? itemName.toString() : capitalizeFully(itemStack.getType().name().replace("_", " ").toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return capitalizeFully(itemStack.getType().name().replace("_", " ").toLowerCase());
    }

    public static String capitalizeFully(String name) {
        if (name != null) {
            if (name.length() > 1) {
                if (name.contains("_")) {
                    StringBuilder sbName = new StringBuilder();
                    for (String subName : name.split("_"))
                        sbName.append(subName.substring(0, 1).toUpperCase() + subName.substring(1).toLowerCase()).append(" ");
                    return sbName.toString().substring(0, sbName.length() - 1);
                } else {
                    return name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
                }
            } else {
                return name.toUpperCase();
            }
        } else {
            return "";
        }
    }
}
