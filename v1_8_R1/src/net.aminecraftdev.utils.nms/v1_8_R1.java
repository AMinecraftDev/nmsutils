package net.aminecraftdev.utils.nms;

import net.aminecraftdev.utils.reflection.ReflectionHandler;
import net.minecraft.server.v1_8_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 09-Nov-17
 */
public class v1_8_R1 implements ReflectionHandler {

    private Field entityWidth, entityLength;

    public v1_8_R1() {
        try {
            entityLength = Entity.class.getField("length");
            entityLength.setAccessible(true);
            entityWidth = Entity.class.getField("width");
            entityWidth.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getVersion() {
        return getClass().getSimpleName();
    }

    @Override
    public void sendActionBar(Player player, String s) {
        if(player == null) return;
        if(s == null) return;

        CraftPlayer craftPlayer = (CraftPlayer) player;

        IChatBaseComponent chatBaseComponent = ChatSerializer.a("{\"text\": \"" + s + "\"}");
        PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(chatBaseComponent);
        PlayerConnection playerConnection = craftPlayer.getHandle().playerConnection;

        playerConnection.sendPacket(packetPlayOutChat);
    }

    @Override
    public void sendActionBar(Player player, String s, int i, Plugin plugin) {
        sendActionBar(player, s);

        if(i >= 0) {
            Bukkit.getScheduler().runTaskLater(plugin, () -> sendActionBar(player, ""), i+1);
        }

        while(i > 60) {
            i -= 60;
            int schedule = i % 60;

            Bukkit.getScheduler().runTaskLater(plugin, () -> sendActionBar(player, s), (long) schedule);
        }
    }

    @Override
    public void sendActionBarAll(String s, Plugin plugin) {
        sendActionBarAll(s, -1, plugin);
    }

    @Override
    public void sendActionBarAll(String s, int i, Plugin plugin) {
        Bukkit.getServer().getOnlinePlayers().forEach(player -> sendActionBar(player, s, i , plugin));
    }

    @Override
    public boolean isChunkLoaded(Location location) {
        if(location == null) return false;
        if(location.getWorld() == null) return false;

        int x = location.getBlockX() / 16;
        int z = location.getBlockZ() / 16;
        CraftWorld craftWorld = (CraftWorld) location.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        ChunkProviderServer chunkProviderServer = worldServer.chunkProviderServer;

        return chunkProviderServer.isChunkLoaded(x, z);
    }

    @Override
    public void loadChunk(Location location) {
        if(isChunkLoaded(location)) return;

        int x = location.getBlockX() / 16;
        int z = location.getBlockZ() / 16;
        CraftWorld craftWorld = (CraftWorld) location.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        ChunkProviderServer chunkProviderServer = worldServer.chunkProviderServer;

        chunkProviderServer.loadChunk(x, z);
    }

    @Override
    public void setSpawnerValues(Block block, int spawnAmount) {
        CraftWorld craftWorld = (CraftWorld) block.getWorld();
        WorldServer worldServer = craftWorld.getHandle();
        TileEntity tileEntity = worldServer.getTileEntity(new BlockPosition(block.getX(), block.getY(), block.getZ()));

        if(!(tileEntity instanceof TileEntityMobSpawner)) return;

        TileEntityMobSpawner tileEntityMobSpawner = (TileEntityMobSpawner) tileEntity;
        NBTTagCompound compound = new NBTTagCompound();

        tileEntityMobSpawner.b(compound);
        compound.setShort("SpawnCount", (short) spawnAmount);
        compound.setShort("MaxNearbyEntities", (short) 5000);
        tileEntityMobSpawner.a(compound);
    }
}